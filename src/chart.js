import Chart from "chart.js/auto";

const context = document.getElementById("chart").getContext("2d");
const socket = new WebSocket("ws://localhost:9000");

const data = [];
const labels = [];

const chart = new Chart(context, {
  type: "line",
  data: {
    labels,
    datasets: [
      {
        label: "Dates and Time",
        data,
        borderColor: "rgb(75, 192, 192)",
      },
    ],
  },
  options: {
    scales: {
      y: {
        min: 0,
        max: 100,
        beginAtZero: true,
      },
    },
  },
});

socket.addEventListener("message", e => {
  const obj = JSON.parse(e.data);
  const xdata = +obj.value;
  const ylabel = obj.time;
  data.labels.push(xdata.value);
  labels.push(ylabel.time);
  chart.update();
});
